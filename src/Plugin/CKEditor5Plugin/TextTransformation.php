<?php

declare(strict_types = 1);

namespace Drupal\ckeditor_text_transformation\Plugin\CKEditor5Plugin;

use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;
use Drupal\editor\EditorInterface;

/**
 * CKEditor 5 Text Transformation plugin.
 */
class TextTransformation extends CKEditor5PluginDefault {

  /**
   * {@inheritdoc}
   */
  public function getDynamicPluginConfig(array $static_plugin_config, EditorInterface $editor): array {
    // Override this method to change the default set of transformations.
    // See https://ckeditor.com/docs/ckeditor5/latest/features/text-transformation.html#configuring-transformations.
    return $static_plugin_config + [
      'typing' => [
        'transformations' => [
          'include' => ['symbols', 'mathematical', 'typography', 'quotes'],
          'remove' => [],
          'extra' => [],
        ],
      ],
    ];
  }

}
